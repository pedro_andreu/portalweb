/**
 * @author victor.gomez@fsanpablo.com
 * Module pattern for the initial config params
 */
var AppConfig = $AC = (function() {
    var webPath = ''; //static files url
    var servicesPath = null; //services context url. If null, it will return the webPath value
    var loginPath = null;
    var servicesUserPath = null;
    var servicesDivisionPath = null;
    var moduleUserPath = null;
    var roles = [];
    var roleCodes = [];
    var allowedActions = []; //Allowed actions
    var locale = null;
    var i18nPath = '/locale'; //path of the i18n file. If null, we won't load it.
    var debug = true;
    var localDataMode = false; //poner a true cuando se quiere trabajar con los archivos de datos estáticos de la carpeta data
    var ajaxTimeout = 60000;

    var getFromCamelCase = function(name) {
        return (name.replace(/([A-Z])/g, "  $1").toLowerCase()).replace(/[a-z]/, function() {
            return arguments[0].toUpperCase();
        });
    };
    //workaround para el problema de console en IE
    console = window.console || {
        log: function() {},
        warn: function() {},
        error: function() {},
        debug: function() {},
        info: function() {}
    };

    return {
        getWebPath: function() {
            return webPath;
        },
        getAjaxTimeout: function() {
            return ajaxTimeout;
        },
        setWebPath: function(wp) {
            webPath = wp;
        },
        getServicesPath: function() {
            return servicesPath !== null ? servicesPath : webPath;
        },
        setServicesPath: function(sp) {
            servicesPath = sp;
        },
        getLoginPath: function() {
            return loginPath !== null ? loginPath : webPath;
        },
        setLoginPath: function(sp) {
            loginPath = sp;
        },
        getServicesUserPath: function() {
            return servicesUserPath !== null ? servicesUserPath : webPath;
        },
        setServicesUserPath: function(sp) {
            servicesUserPath = sp;
        },

        getServicesDivisionPath: function() {
            return servicesDivisionPath !== null ? servicesDivisionPath : webPath;
        },
        setServicesDivisionPath: function(sp) {
            servicesDivisionPath = sp;
        },

        setModuleUserPath: function(mup) {
            moduleUserPath = mup;
        },
        getModuleUserPath: function() {
            return moduleUserPath !== null ? moduleUserPath : webPath;
        },


        setRoleCodes: function(arrRoles) {
            roleCodes = arrRoles;
        },


        setRoles: function(arrRoles) {
            roleCodes = arrRoles;

            var len = arrRoles.length;
            while (len--) {
                roles[arrRoles[len]] = true;
            }
        },

        setAllowedActions: function(arrAllowedActions) {
            var len = arrAllowedActions.length;
            while (len--) {
                allowedActions[arrAllowedActions[len]] = true;
            }
        },

        getAllowedActions: function() {
            return allowedActions;
        },

        getRoleCodes: function() {
            return roleCodes;
        },

        hasRoles: function(role) {
            var len = arguments.length;
            while (len--) {
                if (!roles[arguments[len]]) {
                    return false;
                }
            }
            return true;
        },

        hasAnyRoles: function(role) {
            var len = arguments.length;
            while (len--) {
                if (roles[arguments[len]]) {
                    return true;
                }
            }
            return false;
        },

        getLocale: function() {
            if (locale === null || locale === '') {
                return null;
            }
            return locale.toLowerCase();
        },

        setLocale: function(l) {
            locale = l.toLowerCase();
        },

        debugMode: debug,

        getJSONDateFormat: function() {
            if (localDataMode) {
                return 'd/m/Y';
            } else {
                return 'U';
            }
        },

        isLocalDataMode: function() {
            return localDataMode;
        },

        getUTC: function(d) {
            return Ext.Date.parse(Ext.Date.format(d, "dmYHi") + "+0000", "dmYHiO");
            //return new Date(d.getUTCFullYear(), d.getUTCMonth(), d.getUTCDate(),  d.getUTCHours(), d.getUTCMinutes(), d.getUTCSeconds());
        },

        i18n: null
    };

})();