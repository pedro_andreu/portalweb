<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>
<html> 
<head>
	<meta charset="ISO-8859-1">
	<title>Alta de Item</title>
	<link rel="shortcut icon" href="resources/img/favicon-16x16.png">
	<link rel="stylesheet" type="text/css" href="resources/login/css/bootstrap.min.css">
	<script type="text/javascript" src="resources/login/js/jquery-3.4.1.min.js"></script>
	<link rel="stylesheet" type="text/css" href="resources/login/css/login.css">
	<script type="text/javascript">
		$(document).ready(function(){

			// click on button submit
	        $("#submit").on('click', function(){
	            var email = $("#usuario").val();
	            var password = $("#password").val();

	            //var context = window.location.pathname.substring(0, window.location.pathname.indexOf("/",2)); 
	            var url =window.location.protocol+"//"+ window.location.hostname;// +context+"/bla/bla";
	            //url = url.slice(0, -5);

	            if(email == null  || email == "") {
	            	alert('El campo Usuario es requerido')
		        } else if(password == null || password == "") {
					alert('El campo Contraseña es requerido');
			    } else {
			    	var data = JSON.stringify({email:email, password:password});
		            // send ajax
		            $.ajax({
		            	url: url + '/aitem/authentication/search/user',
		                //url: 'http://localhost:8080/aitem/authentication/search/user', // url where to submit the request
		                type : "POST", // type of action POST || GET
		                dataType : 'json', // data type
		                data : data,
		                //data: formToJson($("form")),
		                contentType: 'application/json;charset=UTF-8',
		                success : function(data) {
		                	console.log(data.id);
		                	//var getInput = prompt(data.id);
		                	//localStorage.setItem("storageName",getInput)

		                	//window.location="index.jsp?param="+data.id;
		                	window.location="index.jsp";
		                	sessionStorage.setItem('user', data.id)
		                },
		                error: function(resp) {
		                    console.log(resp);
		                    alert('Usuario incorrecto, vuelva a intentarlo')
		                }
		            })
				}
	        });
		});
    </script>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body>
	<% 
		String loginAux="loginUser"; 
		session.setAttribute("login",loginAux); 
	%>
	<div class="container h-100">
		<div class="d-flex justify-content-center h-100">
			<div class="form-login">
				<div class="">
					<h3 class="title_login">Bienvenid@ al Servicio Alta de Artículo</h3>
					<!-- <h5 class="title_login">Por favor, elija Tipo de Usuario</h5>  -->
				</div>
				<div class="user_card">
					<div class="d-flex justify-content-center">
						<div class="brand_logo_container">
							<img src="resources/login/img/logo.png" class="brand_logo" alt="Logo">
						</div>
					</div>
					<div class="form_container">
						<form id="form" method="post">
							<div class="input-group login_container">
								<span class="input-group-addon"><span class="glyphicon glyphicon-user" aria-hidden="true"></span></span>
								<input type="text" id="usuario" placeholder="Usuario" name="email" class="form-control"> 
							</div>
							<span class="help-block"></span>					
							<div class="input-group login_container">
								<span class="input-group-addon"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span></span>
								<input type="password" id="password" placeholder="Contraseña" name="password" class="form-control">
							</div>
							<span class="help-block"></span>
							<div class="d-flex justify-content-center mt-3 login_container">
								<input id="submit" type="button" class="btn login_btn" name="submit" value="Iniciar sesión">
								<!-- <button name="button" class="btn login_btn" type="submit">Login</button>  -->
							</div>
						</form>
					</div>
				</div>
				<!--Div donde va a cargar el formulario del ActiveDirectory-->
				<div class="user_interno" id="user_interno">
				</div>
			</div>
		</div>
	</div>
</body>
</html>
